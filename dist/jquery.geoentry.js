/**
 * jquery-geoentry - A jump-start for jQuery plugins development.
 * @version v0.1.0
 * @link http://jqueryboilerplate.com
 * @license MIT
 */
var GoogleGeoCoder = (function () {
    function GoogleGeoCoder() {
    }
    return GoogleGeoCoder;
}());
var $;
var google;
var pluginName = "geoEntry";
var defaults = {
    address: {},
    geoCoder: new GoogleGeoCoder(),
    addressToGeoCoderAddress: $.noop,
    geoCoderAddressToAddress: $.noop,
    flattenAddress: $.noop,
    isComplete: $.noop
};
var GeoEntry = (function () {
    function GeoEntry(element, options) {
        var _this = this;
        this.element = element;
        this._showManualContainer = function (evt) {
            evt.preventDefault();
            _this.autoCompleteContainer.slideUp();
            _this.manualCompleteContainer.slideDown();
            _this.rootEl.trigger('geoentry:viewchange', [{ 'mode': 'manual' }]);
        };
        this._showAutocompleteContainer = function (evt) {
            evt.preventDefault();
            _this.autoCompleteContainer.slideDown();
            _this.manualCompleteContainer.slideUp();
            _this.rootEl.trigger('geoentry:viewchange', [{ 'mode': 'autocomplete' }]);
        };
        this._autoCompletePlaceChanged = function () {
            var place = _this.autocomplete.getPlace();
            if (!place.geometry) {
                window.alert("Autocomplete's returned place contains no geometry");
                return;
            }
            _this.settings.geoCoderAddressToAddress(place, _this._geoCoderAddressToAddressCB);
        };
        this._geoCoderAddressToAddressCB = function (address) {
            _this.autoCompleteContainer.slideUp(400, function () {
                _this.settings.address = address;
                _this.settings.flattenAddress(address, _this._flattenAddressCB);
                _this.rootEl.trigger('geoentry:result', [address]);
                _this._setHiddenFields();
            });
        };
        this._flattenAddressCB = function (flattenAddr) {
            _this.readOnlyAddress.html(flattenAddr);
            _this.readOnlyAddress.append(' ').append(_this._editAddressLink()).slideDown();
            _this.autoCompleteField.val(flattenAddr);
        };
        this._isCompleteCB = function (isComplete) {
            if (true === isComplete) {
                _this.settings.flattenAddress(_this.settings.address, _this._flattenAddressCB);
                _this._setHiddenFields();
            }
            else {
                _this.autoCompleteContainer.slideDown();
            }
        };
        this.settings = $.extend({}, defaults, options);
        this._defaults = defaults;
        this._name = pluginName;
        this.rootEl = $(this.element);
        this.autoCompleteContainer = this.rootEl.find('[data-geoentry="autocomplete-container"]').hide();
        this.manualCompleteContainer = this.rootEl.find('[data-geoentry="manual-container"]').hide();
        this.readOnlyAddress = this.rootEl.find('[data-geoentry="fulladdress"]').hide();
        this._setupAutoComplete();
        this._setupEvents();
        this.init();
    }
    GeoEntry.prototype._setupEvents = function () {
        var showManualLink = this.rootEl.find('[data-geoentry="manual-link"]');
        var showAutocompleteLink = this.rootEl.find('[data-geoentry="autocomplete-link"]');
        showManualLink.click(this._showManualContainer);
        showAutocompleteLink.click(this._showAutocompleteContainer);
    };
    GeoEntry.prototype._setupAutoComplete = function () {
        this.autoCompleteField = this.rootEl.find('[data-geoentry="autocomplete"]');
        var field = this.autoCompleteField[0];
        this.autocomplete = new google.maps.places.Autocomplete(field);
        this.autocomplete.addListener('place_changed', this._autoCompletePlaceChanged);
    };
    GeoEntry.prototype._editAddressLink = function () {
        var _this = this;
        var editLink = $('<a href="#" class="edit-address">Edit</a>');
        editLink.click(function (e) {
            e.preventDefault();
            _this.readOnlyAddress.html('').slideUp(400, function () {
                _this.autoCompleteContainer.slideDown();
            });
        });
        return editLink;
    };
    GeoEntry.prototype.init = function () {
        this.settings.isComplete(this.settings.address, this._isCompleteCB);
    };
    GeoEntry.prototype._setHiddenFields = function () {
        var address = this.settings.address;
        for (var key in address) {
            this.rootEl.find("[data-address='" + key + "']").val(address[key]);
        }
    };
    return GeoEntry;
}());
// A really lightweight plugin wrapper around the constructor,
// preventing against multiple instantiations
$.fn[pluginName] = function (options) {
    return this.each(function () {
        if (!$.data(this, "plugin_" + pluginName)) {
            $.data(this, "plugin_" + pluginName, new GeoEntry(this, options));
        }
    });
};
