namespace geoEntry {
	class GoogleGeoCoder {
		options:any;

		public constructor(options:any) {
			this.options = options;
		}

		public initAutoComplete(pluginObj, autoCompleteField, addressSelectCallback):any {
			let field = autoCompleteField[0];
			let autoComplete = new google.maps.places.Autocomplete(field, this.options);
			autoComplete.addListener('place_changed', addressSelectCallback);
			return autoComplete;
		}
	}
	geoEntry.GoogleGeoCoder = GoogleGeoCoder;
}

var $:any;
var google:any;
let pluginName = "geoEntry";
let defaults = {
	address: {},
	geoCoder: new geoEntry.GoogleGeoCoder({type: [], componentRestrictions: {}}),
	addressToGeoCoderAddress: $.noop,
	geoCoderAddressToAddress: $.noop,
	flattenAddress: $.noop,
	isComplete: $.noop,
};


class GeoEntry {
	private settings:any;
	private _defaults:any;
	private rootEl:any;
	private autoCompleteContainer:any;
	private manualCompleteContainer:any;
	private readOnlyAddress:any;
	private _name:string;
	private autoCompleteField:any;
	private autocomplete:any;
	private readOnlyContainer:any;
	private place: any;

	constructor(private element:any, options:any) {
		this.settings = $.extend({}, defaults, options);
		this._defaults = defaults;
		this._name = pluginName;
		this.rootEl = $(this.element);
		this.autoCompleteContainer = this.rootEl.find('[data-geoentry="autocomplete-container"]').hide();
		this.manualCompleteContainer = this.rootEl.find('[data-geoentry="manual-container"]').hide();
		this.readOnlyContainer = this.rootEl.find('[data-geoentry="readonly-container"]').hide();
		this.readOnlyAddress = this.rootEl.find('[data-geoentry="fulladdress"]');
		this._setupAutoComplete();
		this._setupEvents();
		this.init();
	}


	_showManualContainer = (evt:any) => {
		evt.preventDefault();
		this.autoCompleteContainer.slideUp();
		this.manualCompleteContainer.slideDown();
		this.rootEl.trigger('geoentry:viewchange', [{'mode': 'manual'}]);
	}

	_showAutocompleteContainer = (evt:any) => {
		evt.preventDefault();
		this.autoCompleteContainer.slideDown();
		this.manualCompleteContainer.slideUp();
		this.readOnlyContainer.slideUp();
		this.rootEl.trigger('geoentry:viewchange', [{'mode': 'autocomplete'}]);
	}

	init():any {
		this.settings.isComplete(this.settings.address, this._isCompleteCB);
	}

	_setupEvents() {
		let showManualLink = this.rootEl.find('[data-geoentry="manual-link"]');
		let showAutocompleteLink = this.rootEl.find('[data-geoentry="autocomplete-link"]');
		showManualLink.click(this._showManualContainer);
		showAutocompleteLink.click(this._showAutocompleteContainer);
	}

	_setupAutoComplete() {
		this.autoCompleteField = this.rootEl.find('[data-geoentry="autocomplete"]');
		this.autocomplete = this.settings.geoCoder.initAutoComplete(this, this.autoCompleteField, this.autoCompletePlaceSelected);
	}

	_editAddressLink():any {
		let editLink = this.readOnlyAddress.find('[data-geoentry="autocomplete-link"]');
		editLink.off('click').click((e:any) => {
			e.preventDefault();
			this.readOnlyContainer.slideUp(400, ()=> {
				this.autoCompleteContainer.slideDown();
			});
		});
		return editLink;
	}

	public autoCompletePlaceSelected = ():any => {
		this.place = this.autocomplete.getPlace();
		if (!this.place.geometry) {
			window.alert("Autocomplete's returned place contains no geometry");
			return;
		}

		this.settings.geoCoderAddressToAddress(this.place, this._geoCoderAddressToAddressCB);
	}

	_geoCoderAddressToAddressCB = (address:any) => {
		this.autoCompleteContainer.slideUp(400, () => {
			this.settings.address = address;
			this.settings.flattenAddress(address, this._flattenAddressCB);
			this.rootEl.trigger('geoentry:result', [address, this.place]);
			this._setHiddenFields();
		});
	}

	_flattenAddressCB = (flattenAddr:any) => {
		this._editAddressLink().show();
		this.readOnlyContainer.slideDown();
		this.autoCompleteField.val(flattenAddr);
	}

	_isCompleteCB = (isComplete:boolean) => {
		if (true === isComplete) {
			this.settings.flattenAddress(this.settings.address, this._flattenAddressCB);
			this._setHiddenFields();
		}
		else {
			this.autoCompleteContainer.slideDown();
		}
	}

	_setHiddenFields():any {
		let address = this.settings.address;
		for (var key in address) {
			this.rootEl.find(`[data-address='${key}']`).val(address[key]);
			this.rootEl.find(`[data-address='${key}']`).html(address[key]);
		}
	}
}


// A really lightweight plugin wrapper around the constructor,
// preventing against multiple instantiations
$.fn[pluginName] = function (options:any) {
	return this.each(function () {
		if (!$.data(this, `plugin_${pluginName}`)) {
			$.data(this, `plugin_${pluginName}`, new GeoEntry(this, options));
		}
	});
};
