var geoEntry;
(function (geoEntry) {
    var GoogleGeoCoder = (function () {
        function GoogleGeoCoder(options) {
            this.options = options;
        }
        GoogleGeoCoder.prototype.initAutoComplete = function (pluginObj, autoCompleteField, addressSelectCallback) {
            var field = autoCompleteField[0];
            var autoComplete = new google.maps.places.Autocomplete(field, this.options);
            autoComplete.addListener('place_changed', addressSelectCallback);
            return autoComplete;
        };
        return GoogleGeoCoder;
    }());
    geoEntry.GoogleGeoCoder = GoogleGeoCoder;
})(geoEntry || (geoEntry = {}));
var $;
var google;
var pluginName = "geoEntry";
var defaults = {
    address: {},
    geoCoder: new geoEntry.GoogleGeoCoder({ type: [], componentRestrictions: {} }),
    addressToGeoCoderAddress: $.noop,
    geoCoderAddressToAddress: $.noop,
    flattenAddress: $.noop,
    isComplete: $.noop
};
var GeoEntry = (function () {
    function GeoEntry(element, options) {
        var _this = this;
        this.element = element;
        this._showManualContainer = function (evt) {
            evt.preventDefault();
            _this.autoCompleteContainer.slideUp();
            _this.manualCompleteContainer.slideDown();
            _this.rootEl.trigger('geoentry:viewchange', [{ 'mode': 'manual' }]);
        };
        this._showAutocompleteContainer = function (evt) {
            evt.preventDefault();
            _this.autoCompleteContainer.slideDown();
            _this.manualCompleteContainer.slideUp();
            _this.readOnlyContainer.slideUp();
            _this.rootEl.trigger('geoentry:viewchange', [{ 'mode': 'autocomplete' }]);
        };
        this.autoCompletePlaceSelected = function () {
            _this.place = _this.autocomplete.getPlace();
            if (!_this.place.geometry) {
                window.alert("Autocomplete's returned place contains no geometry");
                return;
            }
            _this.settings.geoCoderAddressToAddress(_this.place, _this._geoCoderAddressToAddressCB);
        };
        this._geoCoderAddressToAddressCB = function (address) {
            _this.autoCompleteContainer.slideUp(400, function () {
                _this.settings.address = address;
                _this.settings.flattenAddress(address, _this._flattenAddressCB);
                _this.rootEl.trigger('geoentry:result', [address, _this.place]);
                _this._setHiddenFields();
            });
        };
        this._flattenAddressCB = function (flattenAddr) {
            _this._editAddressLink().show();
            _this.readOnlyContainer.slideDown();
            _this.autoCompleteField.val(flattenAddr);
        };
        this._isCompleteCB = function (isComplete) {
            if (true === isComplete) {
                _this.settings.flattenAddress(_this.settings.address, _this._flattenAddressCB);
                _this._setHiddenFields();
            }
            else {
                _this.autoCompleteContainer.slideDown();
            }
        };
        this.settings = $.extend({}, defaults, options);
        this._defaults = defaults;
        this._name = pluginName;
        this.rootEl = $(this.element);
        this.autoCompleteContainer = this.rootEl.find('[data-geoentry="autocomplete-container"]').hide();
        this.manualCompleteContainer = this.rootEl.find('[data-geoentry="manual-container"]').hide();
        this.readOnlyContainer = this.rootEl.find('[data-geoentry="readonly-container"]').hide();
        this.readOnlyAddress = this.rootEl.find('[data-geoentry="fulladdress"]');
        this._setupAutoComplete();
        this._setupEvents();
        this.init();
    }
    GeoEntry.prototype.init = function () {
        this.settings.isComplete(this.settings.address, this._isCompleteCB);
    };
    GeoEntry.prototype._setupEvents = function () {
        var showManualLink = this.rootEl.find('[data-geoentry="manual-link"]');
        var showAutocompleteLink = this.rootEl.find('[data-geoentry="autocomplete-link"]');
        showManualLink.click(this._showManualContainer);
        showAutocompleteLink.click(this._showAutocompleteContainer);
    };
    GeoEntry.prototype._setupAutoComplete = function () {
        this.autoCompleteField = this.rootEl.find('[data-geoentry="autocomplete"]');
        this.autocomplete = this.settings.geoCoder.initAutoComplete(this, this.autoCompleteField, this.autoCompletePlaceSelected);
    };
    GeoEntry.prototype._editAddressLink = function () {
        var _this = this;
        var editLink = this.readOnlyAddress.find('[data-geoentry="autocomplete-link"]');
        editLink.off('click').click(function (e) {
            e.preventDefault();
            _this.readOnlyContainer.slideUp(400, function () {
                _this.autoCompleteContainer.slideDown();
            });
        });
        return editLink;
    };
    GeoEntry.prototype._setHiddenFields = function () {
        var address = this.settings.address;
        for (var key in address) {
            this.rootEl.find("[data-address='" + key + "']").val(address[key]);
            this.rootEl.find("[data-address='" + key + "']").html(address[key]);
        }
    };
    return GeoEntry;
}());
// A really lightweight plugin wrapper around the constructor,
// preventing against multiple instantiations
$.fn[pluginName] = function (options) {
    return this.each(function () {
        if (!$.data(this, "plugin_" + pluginName)) {
            $.data(this, "plugin_" + pluginName, new GeoEntry(this, options));
        }
    });
};
