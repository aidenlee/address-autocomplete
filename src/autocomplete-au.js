window.currentAddress = null;
var address = new Array();
var i;
var famousCity;
$(function() {
    $("#addressarea")
        .geoEntry({
            address: {},
            geoCoder: new geoEntry.GoogleGeoCoder({
                types: ["geocode"],
                componentRestrictions: { 'country': 'au' }
            }),
            //geoCoder : new geoEntry.GoogleGeoCoder(), //defaults to google geocoder
            addressToGeoCoderAddress: function(address, callbackFn) {
                // callbackFn(address.street + ',' + address.city + ',' + address.postcode);
                callbackFn(address.street + ',' + address.suburb + ',' + address.city + ',' + address.postcode);
            },
            geoCoderAddressToAddress: function(geoCoderAddress, callbackFn) {
                // use geoCoderAddress object to identify your address components
                address = {
                    streetNumber: '',
                    street: '',
                    city: '',
                    state: '',
                    postcode: ''
                };

                for (var i = 0, len = geoCoderAddress.address_components.length; i < len; i++) {
                    var section = geoCoderAddress.address_components[i];
                    var types = section.types;
                    if (types[0] === 'postal_code') {
                        address.postcode = section.long_name;
                    }
                    if (types[0] === 'administrative_area_level_1') {
                        address.state = section.long_name;
                    }
                    if (types[0] === 'locality') {
                        address.city = section.long_name;
                    }
                    if (types[0] === 'street_number') {
                        address.streetNumber = section.long_name;
                    }
                    if (types[0] === 'route') {
                        address.streetname = section.long_name;
                        address.street = address.streetNumber + ' ' + address.streetname;
                        // console.log(address.street);
                    }
                }
                callbackFn(address);
            },
            flattenAddress: function(address, callbackFn) {
                callbackFn(
                    address.streetNumber + ' ' + address.streetname + ', ' + address.city + ', ' + address.state + ', ' + address.postcode
                );
            },
            isComplete: function(address, callbackFn) {
                //invalid address
                callbackFn(false);
            }
        })
        .on('geoentry:result', function(evt, address, place) {

            // So you should in thery be able to put some code in here
            // console.log(address);
            address.country = "Australia";
            address.regionCode = address.city;

            loadDealer();
            salesAreaChange('#salesAreaCode');
            changeDealers('#salesAreaCode');

            // alert(famousCity);

            // console.log(place.geometry.access_points);
            // var geoMapping = JSON.stringify(place.geometry.access_points[0].location);

            if (place.geometry.access_points !== undefined) {

                var geoMappingLat = place.geometry.access_points[0].location.lat,
                    geoMappingLng = place.geometry.access_points[0].location.lng;

                var point = new google.maps.LatLng(geoMappingLat, geoMappingLng);

                var keyMap = Object.keys(regionMapping);
                var regionPolygons = new Array();

                for (i = 0; i < keyMap.length; i++) {

                    var regionTitle = keyMap[i];
                    var geoMapping = new google.maps.Polygon({ paths: regionMapping[regionTitle] });

                    regionPolygons[regionTitle] = geoMapping;
                }

                // console.log(regionPolygons);

                setTimeout(function() {
                    for (i = 0; i < keyMap.length; i++) {
                        var regionTitle = keyMap[i];
                        var resultColor = google.maps.geometry.poly.containsLocation(point, regionPolygons[regionTitle]) ? 'Inside' : 'Outside region';
                        if (resultColor === 'Inside') {
                            var regionValue = regionTitle;
                            loadDealer();
                            RegionChangeDealers(regionTitle);
                        }
                    }
                }, 500);
            }

        })
        .on('geoentry:viewchange', function(evt, result) {
            console.log(result);

        });
});
