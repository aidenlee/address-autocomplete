### Tasks: ###

gulp serve // for watching file, with live reload. opens browser at http://localhost:3000

gulp dist //create final builds. One unminified and one minified JS. The banner for dist files is loaded from package.json file. Check gulpfile for more

gulp clean // to clean dist folder

gulp ts //to compile .ts file to .js