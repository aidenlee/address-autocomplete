var gulp = require('gulp');
var ts = require('gulp-typescript');
var browserSync = require('browser-sync').create();
var reload      = browserSync.reload;
var clean = require('gulp-clean');
var uglify = require('gulp-uglify');
var header = require('gulp-header');

gulp.task('ts', function () {
	gulp.src('src/*.ts')
		.pipe(ts({
			noImplicitAny: true,
			out: 'jquery.geoentry.js'
		}))
		.pipe(gulp.dest('src'));
});

gulp.task('clean', function () {
	gulp.src('dist/*', {read: false})
		.pipe(clean());
});

// start the development
gulp.task('serve', function () {
	browserSync.init({
		server: {
			baseDir: ["./demo", "./bower_components", "./src"]
		}
	});
	gulp.watch('./src/*.ts', ['ts']);
	gulp.watch(['./src/*.js','./demo/*.html']).on("change", reload);
});


// using data from package.json
var pkg = require('./package.json');
var banner = ['/**',
	' * <%= pkg.name %> - <%= pkg.description %>',
	' * @version v<%= pkg.version %>',
	' * @link <%= pkg.homepage %>',
	' * @license <%= pkg.license %>',
	' */',
	''].join('\n');


gulp.task('dist', function(){
	gulp.start(['clean']);
	gulp.src('src/*.ts')
		.pipe(ts({
			noImplicitAny: true,
			out: 'jquery.geoentry.js'
		}))
		.pipe(header(banner, { pkg : pkg } ))
		.pipe(gulp.dest('dist'));
	gulp.src('src/*.ts')
		.pipe(ts({
			noImplicitAny: true,
			out: 'jquery.geoentry.min.js'
		}))
		.pipe(uglify())
		.pipe(header(banner, { pkg : pkg } ))
		.pipe(gulp.dest('dist'));
});

